from mc.dsa import *
import time
import pickle
import math

k = 7100.0
T = 460.0

A = -1.0/T
B = 1.0 * k/T
C = 2.0         # 1.29
G = 300.0       # 300.0

t = []
vel = []
cur = [] 
dVel = []
sleepTime = 0.001
n = 0

desiredCurrent = 550                # [mA] zadanny prad
actualVel = 0.0
desiredVel = 0.0

d = Dsa(1)                          # wyb�r adresu silnika
d.Disable()                         # zatrzymanie silnika
d.ClrError()                        # wyczyszczenie bledow
d.DefaultParam()                    # ustawienie domysnych parapemtrow
d.MotorTypeDC()                     # ustrawienie typu silnika na dc
d.VelFeedbackEnc()                  # informacja zwrotna z enkodera
d.MotorEncResolution(4*400)         # rozdzielczosc enkodera
d.MotorPolarity(1)                  # wybranie czujnik�w halla
d.ModeCurr()                        # sterowanie pradowe
d.CurrSource(0x200)                 # wybor parametru odpowiedzialnego za zadawanie wartosci pradu
d.FctPosNotationIndex(-3)
d.FctControl(3)

d.CurrKp(30)                        # czlon proporcionalny
d.CurrKi(5)                         # calkujacy
d.CurrLimitMaxNeg(1000)             # maksymalny prad na -
d.CurrLimitMaxPos(1000)             # maksymalny prad na +

d.Curr(desiredCurrent)              # zadanie pradu
d.Enable()                          # uruchomienie silnika

def ramp(n, E):
   a=1/E
   b=0
   return a*n+b


def sign(n):
   E = 800
   if n < E and n > -E:
      return ramp(n, E)
   elif n > E:
      return 1
   else:
      return -1

def control(desired, actual):
   e = actual - desired
   return int(-G * sign(e) - (A/B) * actual - C * e)

while n < 4800:        
   if n < 800:
      desiredVel = desiredVel + 3000.0/800.0
   elif n < 1600:
      desiredVel = 3000.0  
   elif n < 2400:
      desiredVel = desiredVel + 2000.0/800.0 
   elif n < 4000:  
      desiredVel = desiredVel - 7000.0/1600.0
   actualVel = d.MesVel()
   #print "C " + str(desiredCurrent) + " V " + str(actualVel)
   desiredCurrent = control(desiredVel, actualVel)
   d.Curr(desiredCurrent)

   time.sleep(sleepTime)
   t.append(n * sleepTime)
   cur.append(d.ActCurr())  
   vel.append(d.MesVel())
   dVel.append(desiredVel)
   n = n + 1

d.Curr(0)                           # zaadanie pradu na wartosc 0
d.Disable()                         # zatrzymanie silnika

print "t = " + str(t)
print "v = " + str(vel)
print "c = " + str(cur) 
print "dVel = " + str(dVel)

