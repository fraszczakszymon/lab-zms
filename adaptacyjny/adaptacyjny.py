import matplotlib.pyplot as plt
import numpy as np

def generatorTrajektori(t):
    return np.sign(np.sin(1.5*t))

# parametry symulacji
Tp = 10.0**-3
N = 25000
# os czasu dla wykresow
time = np.arange(0,Tp*N,Tp)

# parametry obiekt
a = 4.0
b = 3.0

# parametry modelu referencyjnego
am = 8.0
bm = 8.0

# stan modelu referencyjnego
xm_next = 0.0
xm_curr = 0.0

# stan obiektu sterowania
x_next = 0.0
x_curr = 0.0

# parametry regulatora
th1_prev = 0.0
th1_curr = 0.0

th2_prev = 0.0
th2_curr = 0.0

# wzmocnienia regulatora
g1 = 10.0;
g2 = 20.0;

# sygnaly do wykresu
vct_x = []
vct_xm = []
vct_th1 = []
vct_th2 = []

for n in range(0,N-1):
    # zmiana stanu
    x_curr = x_next
    xm_curr = xm_next
    th1_prev = th1_curr
    th2_prev = th2_curr
    # model referencyjny
    uc = generatorTrajektori(n*Tp)
    xm_next = xm_curr + Tp*(-am*xm_curr + bm*uc);    
    # regulator
    e = x_curr - xm_curr
    th1_curr = th1_prev - Tp*g1*x_curr*e
    th2_curr = th2_prev - Tp*g2*uc*e
    u = th1_curr * x_curr + th2_curr * uc    
    x_next = x_curr + Tp*(-a*x_curr + b*u)
    # wyniki
    vct_x.append(x_curr)
    vct_xm.append(xm_curr)
    vct_th1.append(th1_curr)
    vct_th2.append(th2_curr)

# ostatnia iteracja
vct_x.append(x_next)
vct_xm.append(xm_next)
th1_prev = th1_curr
th2_prev = th2_curr    
vct_th1.append(th1_prev - Tp*g1*x_curr*e)
vct_th2.append(th2_prev - Tp*g2*uc*e)

# idealne wartosci regulatora
print("theta1 (idealna wartosc)= " + repr((a-am)/b))
print("theta1 (estymowana)= " + repr(th1_curr))

print("theta2 (idealna wartosc)= " + repr(am/b))
print("theta2 (estymowana)= " + repr(th2_curr))

# porownanie stanow
ax = plt.subplot(111)        
ax.plot(time, vct_xm, label='model referencyjny')
ax.plot(time, vct_x, label='sterowany obiekt')
plt.xlabel('time $[s]$')
plt.ylabel('$x_m$, $x$')
plt.legend()
plt.show()

# adaptacyja parametrow
ax = plt.subplot(111)        
ax.plot(time, vct_th1, label='parameter $\hat{\\theta}_1$')
ax.plot(time, vct_th2, label='parameter $\hat{\\theta}_2$')
plt.xlabel('time $[s]$')
plt.ylabel('$\hat{\\theta}_1$, $\hat{\\theta}_2$')
plt.legend()
plt.show()


