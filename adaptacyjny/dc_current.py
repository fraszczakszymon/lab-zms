from mc.dsa import *
import time
import pickle
import math

def generatorTrajektori(t):
    return 1000.0*math.sin(8*t - math.pi/2)+4000.0
    #if math.sin(4*t + math.pi/4) > 0:
    #    return 3500.0
    #else:
    #    return 2500.0

# parametry symulacji
Tp = 0.001
N = 5000

# parametry modelu referencyjnego
am = 50.0
bm = 50.0

# stan modelu referencyjnego
xm_next = 0.0
xm_curr = 0.0

# stan obiektu sterowania
x_next = 0.0
x_curr = 0.0

# parametry regulatora
th1_prev = 0.0
th1_curr = 0.0

th2_prev = 0.0
th2_curr = 0.0

# wzmocnienia regulatora
g1 = 0.0000005;
g2 = 0.0000005;

# sygnaly do wykresu
t = []
vel = []
tr = []
model = []
cur = []
th1 = []
th2 = []

# ustawienia silnika
desiredCurrent = 0                  # [mA] zadanny prad
actualVel = 0.0
desiredVel = 0.0

d = Dsa(1)                          # wyb�r adresu silnika
d.Disable()                         # zatrzymanie silnika
d.ClrError()                        # wyczyszczenie bledow
d.DefaultParam()                    # ustawienie domysnych parapemtrow
d.MotorTypeDC()                     # ustrawienie typu silnika na dc
d.VelFeedbackEnc()                  # informacja zwrotna z enkodera
d.MotorEncResolution(4*400)         # rozdzielczosc enkodera
d.MotorPolarity(1)                  # wybranie czujnik�w halla
d.ModeCurr()                        # sterowanie pradowe
d.CurrSource(0x200)                 # wybor parametru odpowiedzialnego za zadawanie wartosci pradu
d.FctPosNotationIndex(-3)
d.FctControl(3)

d.CurrKp(30)                        # czlon proporcionalny
d.CurrKi(5)                         # calkujacy
d.CurrLimitMaxNeg(1000)             # maksymalny prad na -
d.CurrLimitMaxPos(1000)             # maksymalny prad na +

d.Curr(desiredCurrent)              # zadanie pradu
d.Enable()

for n in range(0,N-1):              # uruchomienie silnika
    # zmiana stanu
    x_curr = x_next
    xm_curr = xm_next
    th1_prev = th1_curr
    th2_prev = th2_curr
    # model referencyjny
    uc = generatorTrajektori(n*Tp)
    xm_next = xm_curr + Tp*(-am*xm_curr + bm*uc);
    # regulator
    e = x_curr - xm_curr
    th1_curr = th1_prev - Tp*g1*x_curr*e
    th2_curr = th2_prev - Tp*g2*uc*e
    u = th1_curr * x_curr + th2_curr * uc
    d.Curr(int(u))
    x_next = d.MesVel()
    # wyniki
    t.append(n*Tp)
    vel.append(x_next)
    tr.append(uc)
    model.append(xm_next)
    th1.append(th1_curr)
    th2.append(th2_curr)
    # czekaj
    time.sleep(Tp)

d.Curr(0)                           # zaadanie pradu na wartosc 0
d.Disable()                         # zatrzymanie silnika

print "t = " + str(t)
print "vel = " + str(vel)
print "tr = " + str(tr)
print "model = " + str(model)
print "th1 = " + str(th1)
print "th2 = " + str(th2)

